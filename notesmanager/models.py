from django.db import models
from django.db.models import Sum
from django.utils.translation import ugettext_lazy as _
import datetime


class Materia(models.Model):
    nome = models.CharField(max_length=100, verbose_name=_("nome"))
    in_media = models.BooleanField(default=True, verbose_name=_("voti in media"))

    def __str__(self):
        return self.nome

    def _media(self, voti=None, user=None):
        if not voti:
            voti = Voto.objects.filter(studente=user, materia=self).order_by("data")
        somma = 0
        pesi = 0
        for voto in voti:
            somma += voto.voto * voto.peso
            pesi += voto.peso
        try:
            media = somma/pesi
        except (ZeroDivisionError, TypeError):
            media = 0
        return media

    def get_flot_data(self, user):
        voti = Voto.objects.filter(studente=user, materia=self).order_by("data")
        this = []
        for x in voti:
            epoch = (x.data - datetime.date(1970, 1, 1))/datetime.timedelta(seconds=1)*1000
            this.append([epoch, x.voto])
        somma = voti.aggregate(Sum("voto"))["voto__sum"]
        media = self._media(voti, somma)
        return [self, this, voti, media]


    class Meta:
        verbose_name = _("materia")
        verbose_name_plural = _("materie")


class Voto(models.Model):
    studente = models.ForeignKey("auth.User", verbose_name=_("studente"), on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, verbose_name=_("materia"), on_delete=models.CASCADE)
    data = models.DateField(verbose_name=_("data"), default=datetime.date.today)
    voto = models.FloatField(verbose_name=_("voto"))
    peso = models.FloatField(verbose_name=_("peso (decimale)"), default=1, help_text=_("esprimere in valori da 0 a 1"))
    commento = models.TextField(verbose_name=_("commento"), null=True, blank=True)

    def __str__(self):
        return "{} {} {} {} {}".format(
            self.voto, _("di"), self.materia.nome, _("del"),
            self.data.strftime("%d/%m/%Y")
        )

    class Meta:
        verbose_name = _("voto")
        verbose_name_plural = _("voti")

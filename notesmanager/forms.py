from django import forms
from . import models


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class VotoForm(forms.ModelForm):
    class Meta:
        model = models.Voto
        fields = ["materia", "data", "voto", "peso", "commento"]

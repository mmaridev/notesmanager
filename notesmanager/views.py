from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse_lazy
from django.shortcuts import redirect
from . import forms, models


class IndexView(LoginRequiredMixin, View):
    def __init__(self, *args, **kw):
        self.post = self.get
        return super().__init__(*args, **kw)

    def get(self, request):
        return redirect("home")


class LoginView(FormView):
    template_name = "login.html"
    form_class = forms.LoginForm

    def form_valid(self, form):
        user = authenticate(
            self.request,
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"]
        )
        if user is not None:
            login(self.request, user)
            messages.success(self.request, _("Benvenuto, %s" % user.get_full_name() or user.username))
        else:
            messages.warning(self.request, _("Username o password errati"))
            return self.render_to_response({"form": self.form_class})

        return redirect("home")


class LogoutView(LoginRequiredMixin, View):
    def __init__(self, *args, **kw):
        self.post = self.get
        return super().__init__(*args, **kw)

    def get(self, request):
        logout(request)
        return redirect("home")


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"

    def get_context_data(self):
        materie = []
        for x in models.Materia.objects.all():
            materie.append(x.get_flot_data(self.request.user))
        all = [[]]
        media_compl = 0
        for m in materie:
            if m[3] > 0 and m[0].in_media:
                all[0].append(m[1])
                media_compl += m[3]
        try:
            all.append(media_compl/len(all[0]))
        except ZeroDivisionError:
            all.append(0)
        return {"materie": materie, "all": all}


class NuovoVotoView(LoginRequiredMixin, FormView):
    template_name = "home.html"
    form_class = forms.VotoForm

    def get(self, request):
        return redirect("home")

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.studente = self.request.user
        obj.save()
        messages.success(self.request, _("%s inserito con successo") % obj)
        return redirect("home")


class ModificaVotoView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = "modifica_voto.html"
    model = models.Voto
    form_class = forms.VotoForm
    success_url = reverse_lazy("home")
    success_message = _("Voto modificato con successo")


class CancellaVotoView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    template_name = "cancella_voto.html"
    model = models.Voto
    form_class = forms.VotoForm
    success_url = reverse_lazy("home")
    success_message = _("Voto eliminato con successo")


class Pagella(LoginRequiredMixin, TemplateView):
    template_name = "pagella.html"

    def get_context_data(self):
        totint = 0
        totfloat = 0
        valide = 0
        mats = []
        for x in models.Materia.objects.filter(in_media=True):
            x.media = x._media(user=self.request.user)
            x.finale = round(x.media)
            totint += x.finale
            totfloat += x.media
            if x.finale != 0:
                valide += 1
            mats.append(x)
        return {"materie": mats, "mediatotintera": totint/valide, "mediatotfloat": totfloat/valide}

from django.contrib import admin
from .models import Materia, Voto

admin.site.register(Materia)
admin.site.register(Voto)
